/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_TRANSFORMERS_H__
#define __GOP_TRANSFORMERS_H__

#include <glib-object.h>

void gop_transform__gchararray_gboolean (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gchar (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gdouble (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gfloat (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gint (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gint64 (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_glong (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_guchar (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_guint (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_guint64 (const GValue *src_value, GValue *dest_value);
void gop_transform__gchararray_gulong (const GValue *src_value, GValue *dest_value);

#endif /* __GOP_TRANSFORMERS_H__ */
