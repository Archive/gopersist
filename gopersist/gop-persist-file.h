/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_PERSIST_FILE_H__
#define __GOP_PERSIST_FILE_H__

#include <glib.h>
#include <glib-object.h>
#include <gopersist/gop-persist.h>

G_BEGIN_DECLS

#define GOP_TYPE_PERSIST_FILE                  (gop_persist_file_get_type ())
#define GOP_PERSIST_FILE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_PERSIST_FILE, GopPersistFile))
#define GOP_PERSIST_FILE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_PERSIST_FILE, GopPersistFileClass))
#define GOP_IS_PERSIST_FILE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_PERSIST_FILE))
#define GOP_IS_PERSIST_FILE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_PERSIST_FILE))
#define GOP_PERSIST_FILE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GOP_TYPE_PERSIST_FILE, GopPersistFileClass))

typedef struct _GopPersistFile GopPersistFile;
typedef struct _GopPersistFilePriv GopPersistFilePriv;
typedef struct _GopPersistFileClass GopPersistFileClass;

struct _GopPersistFile {
	GObject parent;
	GopPersistFilePriv *priv;
};

struct _GopPersistFileClass {
	GObjectClass parent;
};

GType gop_persist_file_get_type (void);

GopPersist* gop_persist_file_new (gchar const *file_name);

G_END_DECLS

#endif /* __GOP_PERSIST_FILE_H__ */
