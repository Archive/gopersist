/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gop-xml-sink.h"
#include "gop-xml-sink-internal.h"

enum {
	DONE,
	_LAST_SIGNAL_
};

static guint gop_xml_sink_signals[_LAST_SIGNAL_] = {0};

static void
base_init (gpointer iface)
{
	static gboolean initialized = FALSE;

	if (initialized) {
		return;
	}

	gop_xml_sink_signals [DONE] =
		g_signal_new (
			"done",
			G_TYPE_FROM_INTERFACE (iface),
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (GopXMLSinkClass, done),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);


	initialized = TRUE;
}

GType
gop_xml_sink_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GopXMLSinkClass),
			base_init,   /* base_init */
			NULL,   /* base_finalize */
			NULL,   /* class_init */
			NULL,   /* class_finalize */
			NULL,   /* class_data */
			0,
			0,      /* n_preallocs */
			NULL    /* instance_init */
		};
		type = g_type_register_static (G_TYPE_INTERFACE, "GopXMLSink", 
					       &info, (GTypeFlags)0);
	}
	return type;
}

/**
 * gop_xml_sink_start_element:
 * @self: Self instance.
 * @tag: Tag-name of node.
 * @attr_names: array of attribute names.
 * @attr_values: array of attribute values.
 * @error: Error pointer.
 * 
 * Opening tag read from XML file.
 */
void 
gop_xml_sink_start_element (GopXMLSink   *self,
			    const gchar  *tag,
			    const gchar **attr_names,
			    const gchar **attr_values,
			    GError      **error)
{
	GOP_XML_SINK_GET_CLASS (self)->start_element (self, tag, attr_names, attr_values, error);
}

/**
 * gop_xml_sink_end_element:
 * @self: Self instance.
 * @tag: Tag-name of node.
 * @error: Error pointer.
 *
 * Closing tag read from XML file.
 */
void 
gop_xml_sink_end_element (GopXMLSink          *self,
			  const gchar         *tag,
			  GError             **error)
{
	GOP_XML_SINK_GET_CLASS (self)->end_element (self, tag, error);
}

/**
 * gop_xml_sink_text:
 * @self: Self instance.
 * @text: Text read, unterminated string.
 * @text_len: Length of text.
 * @error: Error pointer.
 *
 * Text read from XML file.
 */
void 
gop_xml_sink_text (GopXMLSink   *self,
		   const gchar  *text,
		   gsize         text_len,  
		   GError      **error)
{
	GOP_XML_SINK_GET_CLASS (self)->text (self, text, text_len, error);
}

/**
 * gop_xml_sink_get_object:
 * @self: Self instance.
 *
 * Get a reference to the object instantiated by the implementing sink.
 * 
 * Returns: Object instance, <literal>g_object_unref()</literal> when no longer referenced.
 */
GObject* 
gop_xml_sink_get_object (GopXMLSink *self)
{
	return GOP_XML_SINK_GET_CLASS (self)->get_object (self);
}

/**
 * gop_xml_sink_get_object_id:
 * @self: Self instance.
 *
 * Get the unique ID of the object instantiated by the implementing sink.
 * 
 * Returns: Object ID, free after use.
 */
gchar* 
gop_xml_sink_get_object_id (GopXMLSink *self)
{
	return GOP_XML_SINK_GET_CLASS (self)->get_object_id (self);
}

/**
 * gop_xml_sink_emit_done:
 * @self: Self instance.
 *
 * Emit the &quot;deserialisation done&quot; signal. For use by XML sinks with implement this 
 * interface only.
 */
void 
gop_xml_sink_emit_done (GopXMLSink *self)
{
	g_signal_emit (self, gop_xml_sink_signals[DONE], 0);
}
