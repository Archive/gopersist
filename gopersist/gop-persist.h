/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_PERSIST_H__
#define __GOP_PERSIST_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define GOP_TYPE_PERSIST                  (gop_persist_get_type ())
#define GOP_PERSIST(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_PERSIST, GopPersist))
#define GOP_PERSIST_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_PERSIST, GopPersistClass))
#define GOP_IS_PERSIST(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_PERSIST))
#define GOP_IS_PERSIST_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_PERSIST))
#define GOP_PERSIST_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GOP_TYPE_PERSIST, GopPersistClass))

typedef struct _GopPersist GopPersist;
typedef struct _GopPersistClass GopPersistClass;

struct _GopPersist {
	GObject parent;
};

struct _GopPersistClass {
	GTypeInterface parent;
	/* vfuncs */
	void     (*register_object) (GopPersist *self, 
				     gchar const *key, 
				     GObject *object);
	void	 (*unregister_object) (GopPersist *self, 
				       gchar const *key);
	GObject* (*get_object) (GopPersist *self, 
				gchar const *key);
	void     (*load) (GopPersist *self);
	void     (*save) (GopPersist *self);
};

GType gop_persist_get_type (void);

void gop_persist_register_object (GopPersist *self, gchar const *key, GObject *object);
void gop_persist_unregister_object (GopPersist *self, gchar const *key);
GObject* gop_persist_get_object (GopPersist *self, gchar const *key);

void gop_persist_load (GopPersist *self);
void gop_persist_save (GopPersist *self);

G_END_DECLS

#endif /* __GOP_PERSIST_H__ */
