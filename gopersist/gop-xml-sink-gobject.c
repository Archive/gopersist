/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gstdio.h>
#include "gop-xml-sink-gobject.h"
#include "gop-xml-sink-internal.h"

/* GopXMLSink interface */
void 
gop_xml_sink_gobject_start_element (GopXMLSinkGObject *self,
				    const gchar       *tag,
				    const gchar      **attr_names,
				    const gchar      **attr_values,
				    GError           **error);
void 
gop_xml_sink_gobject_end_element (GopXMLSinkGObject *self,
				  const gchar       *tag,
				  GError           **error);
void 
gop_xml_sink_gobject_text (GopXMLSinkGObject *self,
			   const gchar       *text,
			   gsize              text_len,  
			   GError           **error);
GObject* gop_xml_sink_gobject_get_object (GopXMLSinkGObject *self);
gchar* gop_xml_sink_gobject_get_object_id (GopXMLSinkGObject *self);

struct _GopXMLSinkGObjectPriv {
	/* object level */
	gchar  *object_id;
	GType   object_type;
	GSList *object_props;
	guint   n_object_props;
	/* property level */
	gchar  *prop_name;
	GType   prop_type;
	GValue *prop_strval;
	/* instantiated object */
	GObject *object;
};

static GObjectClass *gop_xml_sink_gobject_parent_class = NULL;

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GopXMLSinkClass *klass = (GopXMLSinkClass *) iface;

	klass->start_element = (void (*)(GopXMLSink   *self,
					 const gchar  *tag,
					 const gchar **attr_names,
					 const gchar **attr_values,
					 GError      **error))
			       gop_xml_sink_gobject_start_element;

	klass->end_element = (void (*)(GopXMLSink   *self,
				       const gchar  *tag,
				       GError      **error))
			     gop_xml_sink_gobject_end_element;

	klass->text = (void (*)(GopXMLSink   *self,
				const gchar  *text,
				gsize         text_len,  
				GError      **error))
		      gop_xml_sink_gobject_text;
	
	klass->get_object = (GObject* (*)(GopXMLSink *self))
			    gop_xml_sink_gobject_get_object;

	klass->get_object_id = (gchar* (*)(GopXMLSink *self))
			       gop_xml_sink_gobject_get_object_id;
}

static void
instance_init (GopXMLSinkGObject *self)
{
	self->priv = g_new0 (GopXMLSinkGObjectPriv, 1);

	self->priv->prop_strval = g_new0 (GValue, 1);
	g_value_init (self->priv->prop_strval, G_TYPE_STRING);
}

static void
instance_dispose (GObject *instance)
{
	GopXMLSinkGObject *self = (GopXMLSinkGObject*) instance;

	if (self->priv == NULL)
		return;

	/* TODO clean up */

	if (self->priv->object) {
		g_object_unref (self->priv->object);
		self->priv->object = NULL;
	}

	if (self->priv->object_id) {
		g_free (self->priv->object_id);
		self->priv->object_id = NULL;
	}

	g_free (self->priv->prop_strval);
	self->priv->prop_strval = NULL;

	g_free (self->priv);
	self->priv = NULL;

	gop_xml_sink_gobject_parent_class->dispose (G_OBJECT (self));
}

static void
class_init (GopXMLSinkGObjectClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gop_xml_sink_gobject_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);
}

GType
gop_xml_sink_gobject_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GopXMLSinkGObjectClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GopXMLSinkGObject),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo xml_sink_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GopXMLSinkGObject", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GOP_TYPE_XML_SINK, &xml_sink_info);
        }
        return type;
}

/**
 * gop_xml_sink_gobject_new: 
 * 
 * Creates an XML accumulator instance for reading generic gobjects. Internal use only.
 * 
 * Returns: GObject XML accumulator instance.
 */
GopXMLSink* 
gop_xml_sink_gobject_new (void)
{
	return (GopXMLSink*) g_object_new (GOP_TYPE_XML_SINK_GOBJECT, NULL);
}

void 
gop_xml_sink_gobject_start_element (GopXMLSinkGObject *self,
				    const gchar       *tag,
				    const gchar      **attr_names,
				    const gchar      **attr_values,
				    GError           **error)
{
	if (*tag == 'o') {

		/* g_printf ("%s = %s\n", attr_names[0], attr_values[0]); */
		self->priv->object_id = g_strdup (attr_values[0]);
		self->priv->object_type = g_type_from_name (attr_values[1]);
		self->priv->n_object_props = 0;
		self->priv->object_props = NULL;
	}
	else if (*tag == 'p') {
		
		/*
		g_printf ("%s = %s\n", attr_names[0], attr_values[0]);
		g_printf ("%s = %s\n", attr_names[1], attr_values[1]);
		*/
		self->priv->prop_name = g_strdup (attr_values[0]);
		self->priv->prop_type = g_type_from_name (attr_values[1]);
	}
	else {
		g_warning ("%s: ignoring unknown tag '%s'", __FUNCTION__, tag);
	}
}

void 
gop_xml_sink_gobject_end_element (GopXMLSinkGObject *self,
				  const gchar       *tag,
				  GError           **error)
{
	if (*tag == 'o') {

		/* instantiate object */
		GParameter *params = g_new0 (GParameter, self->priv->n_object_props);
		GParameter *param_ptr = params;
		GSList *item = self->priv->object_props;
		gint i;

		while (item) {

			memcpy (param_ptr, item->data, sizeof (GParameter));
			g_free (item->data);

			param_ptr++;
			item = item->next;
		}
		param_ptr = NULL;

		self->priv->object = (GObject*) g_object_newv (self->priv->object_type, 
							       self->priv->n_object_props, 
							       params);
		
		gop_xml_sink_emit_done (GOP_XML_SINK (self));

		/* free params allocated for g_object_newv */
		for (i = 0; i < self->priv->n_object_props; i++) {
			g_free ((gchar*)params[i].name);
			g_value_unset (&params[i].value);
		}
		g_free (params);
		params = NULL;
		
		if (self->priv->object_props) {
			g_slist_free (self->priv->object_props);
			self->priv->n_object_props = 0;
			self->priv->object_props = NULL;
		}
	}
	else if (*tag == 'p') {

		/* transform property to original type and store in param list */

		GParameter *param = NULL;

		if (!g_value_type_transformable (G_TYPE_STRING, self->priv->prop_type)) {
			g_error (g_strdup_printf ("Cannot convert from %s to %s\n", 
						  g_type_name (G_TYPE_STRING), 
						  g_type_name (self->priv->prop_type)));
		}

		param = (GParameter*) g_new0 (GParameter, 1);
		param->name = self->priv->prop_name;

		g_value_init (&param->value, self->priv->prop_type);
		g_value_transform (self->priv->prop_strval, &param->value);

		self->priv->n_object_props++;
		self->priv->object_props = g_slist_prepend (self->priv->object_props, param);
		g_value_reset (self->priv->prop_strval);
	}
	else {
		g_warning ("%s: ignoring unknown tag '%s'", __FUNCTION__, tag);
	}

}

void 
gop_xml_sink_gobject_text (GopXMLSinkGObject *self,
			   const gchar       *text,
			   gsize              text_len,  
			   GError           **error)
{
	if (self->priv->prop_name != NULL) {

		/* parsing properties */

		gchar *dup_text = NULL;

		dup_text = g_strndup (text, text_len);
		g_value_take_string (self->priv->prop_strval, dup_text);
	}	
	else if (text_len > 0) {
		g_warning ("%s: ignoring text because prop_name == NULL", __FUNCTION__);
	}
}

GObject* 
gop_xml_sink_gobject_get_object (GopXMLSinkGObject *self)
{
	g_object_ref (self->priv->object);
	return self->priv->object;
}

gchar* 
gop_xml_sink_gobject_get_object_id (GopXMLSinkGObject *self)
{
	return g_strdup (self->priv->object_id);
}
