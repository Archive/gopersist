/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_PERSIST_GCONF_H__
#define __GOP_PERSIST_GCONF_H__

#include <glib.h>
#include <glib-object.h>
#include <gconf/gconf-client.h>

G_BEGIN_DECLS

#define GOP_TYPE_PERSIST_GCONF                  (gop_persist_gconf_get_type ())
#define GOP_PERSIST_GCONF(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_PERSIST_GCONF, GopPersistGConf))
#define GOP_PERSIST_GCONF_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_PERSIST_GCONF, GopPersistGConfClass))
#define GOP_IS_PERSIST_GCONF(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_PERSIST_GCONF))
#define GOP_IS_PERSIST_GCONF_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_PERSIST_GCONF))
#define GOP_PERSIST_GCONF_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GOP_TYPE_PERSIST_GCONF, GopPersistGConfClass))

typedef struct _GopPersistGConf GopPersistGConf;
typedef struct _GopPersistGConfPriv GopPersistGConfPriv;
typedef struct _GopPersistGConfClass GopPersistGConfClass;

struct _GopPersistGConf {
	GObject parent;
	GopPersistGConfPriv *priv;
};

struct _GopPersistGConfClass {
	GObjectClass parent;
};

GopPersist* gop_persist_gconf_new (gchar const *domain);
GopPersist* gop_persist_gconf_new_with_client (gchar const *domain, GConfClient *client);

G_END_DECLS

#endif /* __GOP_PERSIST_GCONF_H__ */
