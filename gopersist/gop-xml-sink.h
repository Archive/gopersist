/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_XML_SINK_H__
#define __GOP_XML_SINK_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define GOP_TYPE_XML_SINK                  (gop_xml_sink_get_type ())
#define GOP_XML_SINK(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_XML_SINK, GopXMLSink))
#define GOP_XML_SINK_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_XML_SINK, GopXMLSinkClass))
#define GOP_IS_XML_SINK(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_XML_SINK))
#define GOP_IS_XML_SINK_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_XML_SINK))
#define GOP_XML_SINK_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GOP_TYPE_XML_SINK, GopXMLSinkClass))

typedef struct _GopXMLSink GopXMLSink;
typedef struct _GopXMLSinkClass GopXMLSinkClass;

struct _GopXMLSink {
	GObject parent;
};

struct _GopXMLSinkClass {
	GTypeInterface parent;
	/* vfuncs */
	void 
	(*start_element) (GopXMLSink   *self,
			  const gchar  *tag,
			  const gchar **attr_names,
			  const gchar **attr_values,
			  GError      **error);
	void 
	(*end_element) (GopXMLSink   *self,
			const gchar  *tag,
			GError      **error);
	void 
	(*text) (GopXMLSink   *self,
		 const gchar  *text,
		 gsize         text_len,  
		 GError      **error);
	GObject* (*get_object) (GopXMLSink *self);
	gchar* (*get_object_id) (GopXMLSink *self);
	/* signals */
	void (* done) (GopXMLSink *self);
};

GType gop_xml_sink_get_type (void);

void 
gop_xml_sink_start_element (GopXMLSink   *self,
			    const gchar  *tag,
			    const gchar **attr_names,
			    const gchar **attr_values,
			    GError      **error);
void 
gop_xml_sink_end_element (GopXMLSink   *self,
			  const gchar  *tag,
			  GError      **error);
void 
gop_xml_sink_text (GopXMLSink   *self,
		   const gchar  *text,
		   gsize         text_len,  
		   GError      **error);
GObject* gop_xml_sink_get_object (GopXMLSink *self);
gchar* gop_xml_sink_get_object_id (GopXMLSink *self);

G_END_DECLS

#endif /* __GOP_XML_SINK_H__ */
