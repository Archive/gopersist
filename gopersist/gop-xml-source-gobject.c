/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gstdio.h>
#include "gop-xml-source-gobject.h"

/* GopXMLSource interface */
void gop_xml_source_gobject_set_object (GopXMLSourceGObject *self, gchar const *id, GObject *object);
gchar *gop_xml_source_gobject_get_next_part (GopXMLSourceGObject *self);
void gop_xml_source_gobject_rewind (GopXMLSourceGObject *self);

struct _GopXMLSourceGObjectPriv {
	GObject *object;
	GSList *strlist;
	GSList *item;
};

static GObjectClass *gop_xml_source_gobject_parent_class = NULL;

static void
free_strlist (GopXMLSourceGObject *self) 
{
	if (self->priv->strlist) {
		GSList *item = self->priv->strlist;
		while (item) {
			g_free (item->data);
			item->data = NULL;
			item = item->next;
		}
		g_slist_free (self->priv->strlist);
		self->priv->strlist = NULL;
	}
}

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GopXMLSourceClass *klass = (GopXMLSourceClass *) iface;

	klass->set_object = (void (*)(GopXMLSource *self, gchar const *id, GObject *object))
			    gop_xml_source_gobject_set_object;

	klass->get_next_part = (gchar* (*)(GopXMLSource *self))
				gop_xml_source_gobject_get_next_part;

	klass->rewind = (void (*)(GopXMLSource *self))
			 gop_xml_source_gobject_rewind;
}

static void
instance_init (GopXMLSourceGObject *self)
{
	self->priv = g_new0 (GopXMLSourceGObjectPriv, 1);
}

static void
instance_dispose (GObject *instance)
{
	GopXMLSourceGObject *self = (GopXMLSourceGObject*) instance;

	if (self->priv == NULL)
		return;

	if (self->priv->object) {
		g_object_unref (self->priv->object);
		self->priv->object = NULL;
	}

	free_strlist (self);

	g_free (self->priv);
	self->priv = NULL;

	gop_xml_source_gobject_parent_class->dispose (G_OBJECT (self));
}

static void
class_init (GopXMLSourceGObjectClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gop_xml_source_gobject_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);
}

GType
gop_xml_source_gobject_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GopXMLSourceGObjectClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GopXMLSourceGObject),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo xml_source_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GopXMLSourceGObject", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GOP_TYPE_XML_SOURCE, &xml_source_info);
        }
        return type;
}

/**
 * gop_xml_source_gobject_new: 
 * 
 * Creates an XML serialiser instance for writing generic gobjects. Internal use only.
 * 
 * Returns: GObject XML serialiser instance.
 */
GopXMLSource* 
gop_xml_source_gobject_new (void)
{
	return (GopXMLSource*) g_object_new (GOP_TYPE_XML_SOURCE_GOBJECT, NULL);
}

static GSList* 
list_properties (GObjectClass *klass)
{
	GParamSpec **props = NULL;
	GSList *list = NULL;
	guint n_props;
	gint i;

	props = g_object_class_list_properties (klass, &n_props);

	for (i = 0; i < n_props; i++) {
		
		GParamSpec *prop = props[i];
		gboolean is_transformable;
		gboolean is_rwc;		/* Read Write Construct */

		is_transformable = g_value_type_transformable (prop->value_type, 
							       G_TYPE_STRING);
		is_rwc	= prop->flags & (G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

		if (is_transformable && is_rwc) {
			list = g_slist_prepend (list, props[i]);
		}
	}

	return list;
}

void
gop_xml_source_gobject_set_object (GopXMLSourceGObject *self, 
				   gchar const         *id, 
				   GObject             *object)
{
	GSList *proplist = NULL;
	GSList *item = NULL;
	GObjectClass *klass = NULL;


	if (self->priv->object) {
		g_object_unref (self->priv->object);
		self->priv->object = NULL;
	}

	free_strlist (self);

	self->priv->object = object;
	g_object_ref (self->priv->object);


	klass = G_OBJECT_GET_CLASS (object);
	proplist = list_properties (klass);
	
	item = proplist;
	if (item) {

		GParamSpec *spec = NULL;
		GValue val = {0};
		GValue strval = {0};
		gchar *prop = NULL;

		/* prepending items, means closing tag goes here */
		self->priv->strlist = g_slist_prepend (self->priv->strlist, g_strdup ("</o>"));

		do {
			spec = (GParamSpec*) item->data;
			
			g_value_init (&val, spec->value_type);
			g_value_init (&strval, G_TYPE_STRING);
			g_object_get_property (object, spec->name, &val);

			g_value_transform (&val, &strval);
			prop = g_strdup_printf ("<p n=\"%s\" t=\"%s\">%s</p>", 
						spec->name, 
						g_type_name (spec->value_type),
						g_value_get_string (&strval));

			self->priv->strlist = g_slist_prepend (self->priv->strlist, prop);

			g_value_unset (&val);
			g_value_unset (&strval);
	
		} while (NULL != (item = item->next));

		/* starting tag */
		prop = g_strdup_printf ("<o n=\"%s\" t=\"%s\">", 
					id, 
					g_type_name (G_OBJECT_CLASS_TYPE (klass)));
		self->priv->strlist = g_slist_prepend (self->priv->strlist, prop);

		g_slist_free (proplist);
		proplist = NULL;
		item = NULL;
	}


	gop_xml_source_gobject_rewind (self);
}

gchar*
gop_xml_source_gobject_get_next_part (GopXMLSourceGObject *self)
{
	gchar *ret = NULL;

	if (self->priv->item) {
		ret = g_strdup ((gchar*) self->priv->item->data);
		self->priv->item = self->priv->item->next;
	}
	
	return ret;
}

void 
gop_xml_source_gobject_rewind (GopXMLSourceGObject *self)
{
	self->priv->item = self->priv->strlist;
}
