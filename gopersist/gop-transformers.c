/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gop-transformers.h"

/**
 * gop_transform__gchararray_gboolean:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gboolean.
 */
void 
gop_transform__gchararray_gboolean (const GValue *src_value, 
				    GValue *dest_value)
{
	const gchar *src_str = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	/* dest_value must already be initialized 
	if (!G_IS_VALUE (dest_value)) {
		g_value_init (dest_value, G_TYPE_BOOLEAN);
	}
	*/

	src_str = g_value_get_string (src_value);
	if (0 == strcmp (src_str, "FALSE") ||
	    (src_str && *src_str == '0')) {

		g_value_set_boolean (dest_value, FALSE);
	}
	else {
		g_value_set_boolean (dest_value, TRUE);
	}
}

/**
 * gop_transform__gchararray_gchar:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gchar.
 */
void 
gop_transform__gchararray_gchar (const GValue *src_value, 
				 GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);
	g_value_set_char (dest_value, (gchar)val);
}

/**
 * gop_transform__gchararray_gdouble:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gdouble.
 */
void 
gop_transform__gchararray_gdouble (const GValue *src_value, 
				   GValue *dest_value)
{
	const gchar *src_str = NULL;
	gdouble val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtod (src_str, &tailptr);
	g_value_set_double (dest_value, val);

	/*
	if (tailptr != NULL) {
		g_warning (tailptr);
	}
	*/
}

/**
 * gop_transform__gchararray_gfloat:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gfloat.
 */
void 
gop_transform__gchararray_gfloat (const GValue *src_value, 
				  GValue *dest_value)
{
	const gchar *src_str = NULL;
	gdouble val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtod (src_str, &tailptr);
	g_value_set_float (dest_value, (gfloat)val);

	/*
	if (tailptr != NULL) {
		g_warning (tailptr);
	}
	*/
}

/**
 * gop_transform__gchararray_gint:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gint.
 */
void 
gop_transform__gchararray_gint (const GValue *src_value, 
				GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);
	g_value_set_int (dest_value, (gint)val);
}

/**
 * gop_transform__gchararray_gint64:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gint64.
 */
void 
gop_transform__gchararray_gint64 (const GValue *src_value, 
				  GValue *dest_value)
{
	const gchar *src_str = NULL;
	long long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	/* FIXME this is C99 */
	val = strtoll (src_str, &tailptr, 0);
	/* sscanf (src_str, "%Ld", &val); */
	g_value_set_int64 (dest_value, (gint64)val);
}

/**
 * gop_transform__gchararray_glong:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to glong.
 */
void 
gop_transform__gchararray_glong (const GValue *src_value, 
				 GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);
	/* sscanf (src_str, "%ld", &val); */
	g_value_set_long (dest_value, (glong)val);
}

/**
 * gop_transform__gchararray_guchar:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to guchar.
 */
void 
gop_transform__gchararray_guchar (const GValue *src_value, 
				  GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);	
	g_value_set_uchar (dest_value, (guchar)val);
}

/**
 * gop_transform__gchararray_guint:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to guint.
 */
void 
gop_transform__gchararray_guint (const GValue *src_value, 
				 GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);	
	g_value_set_uint (dest_value, (guint)val);
}

/**
 * gop_transform__gchararray_guint64:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to guint64.
 */
void 
gop_transform__gchararray_guint64 (const GValue *src_value, 
				   GValue *dest_value)
{
	const gchar *src_str = NULL;
	long long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	/* FIXME this is C99 */
	val = strtoll (src_str, &tailptr, 0);
	/* sscanf (src_str, "%Ld", &val); */
	g_value_set_uint64 (dest_value, (guint64)val);
}

/**
 * gop_transform__gchararray_gulong:
 * @src_value: 
 * @dest_value: 
 * 
 * Convert gchar* to gulong.
 */
void 
gop_transform__gchararray_gulong (const GValue *src_value, 
				  GValue *dest_value)
{
	const gchar *src_str = NULL;
	long int val;
	gchar *tailptr = NULL;

	g_assert (G_VALUE_HOLDS (src_value, G_TYPE_STRING));

	src_str = g_value_get_string (src_value);
	val = strtol (src_str, &tailptr, 0);	
	g_value_set_ulong (dest_value, (gulong)val);
}
