/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>
#include <gopersist/gopersist.h>
#include <gopersist/gop-xml-sink-registry.h>
#include <gopersist/gop-xml-source-registry.h>
#include "gopersist-gtk.h"
#include "gop-xml-sink-gtkliststore.h"
#include "gop-xml-source-gtkliststore.h"

static gboolean gop_gtk_is_initialized = FALSE;

/**
 * gopersist_gtk_init: 
 * 
 * Initialise the gopersist library with gtk support. Mandatory before 
 * calling any other functions.
 */ 
void 
gopersist_gtk_init (int *argc, 
		    char **argv[])
{
	GopXMLSink *gtkliststore_sink = NULL;
	GopXMLSource *gtkliststore_source = NULL;

	if (gop_gtk_is_initialized) {
		return;
	}

	gtk_init (argc, argv);

	gopersist_init ();

	gtkliststore_sink = gop_xml_sink_gtkliststore_new ();
	gop_xml_sink_registry_register ("GtkListStore", 
					gtkliststore_sink);
	g_object_unref (gtkliststore_sink);

	gtkliststore_source = gop_xml_source_gtkliststore_new ();
	gop_xml_source_registry_register ("GtkListStore", 
					  gtkliststore_source);
	g_object_unref (gtkliststore_source);

	gop_gtk_is_initialized = TRUE;
}

/**
 * gopersist_gtk_shutdown:
 * 
 * Shutdown the gopersist library with gtk support.
 */
void 
gopersist_gtk_shutdown (void)
{
	gopersist_shutdown ();

	/* seems there is no way to unregister transform funcs (yet?) */
}
