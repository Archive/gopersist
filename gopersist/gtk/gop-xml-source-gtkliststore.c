/* GtkListStore persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include "gop-xml-source-gtkliststore.h"

/* GopXMLSource interface */
void gop_xml_source_gtkliststore_set_object (GopXMLSourceGtkListStore *self, gchar const *id, GtkListStore *object);
gchar* gop_xml_source_gtkliststore_get_next_part (GopXMLSourceGtkListStore *self);
void gop_xml_source_gtkliststore_rewind (GopXMLSourceGtkListStore *self);

struct _GopXMLSourceGtkListStorePriv {
	GtkListStore *store;
	GSList *strlist;
	GSList *item;
};

static GObjectClass *gop_xml_source_gtkliststore_parent_class = NULL;

static void
free_strlist (GopXMLSourceGtkListStore *self) 
{
	if (self->priv->strlist) {
		GSList *item = self->priv->strlist;
		while (item) {
			g_free (item->data);
			item->data = NULL;
			item = item->next;
		}
		g_slist_free (self->priv->strlist);
		self->priv->strlist = NULL;
	}
}

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GopXMLSourceClass *klass = (GopXMLSourceClass *) iface;

	klass->set_object = (void (*)(GopXMLSource *self, gchar const *id, GObject *object))
			    gop_xml_source_gtkliststore_set_object;

	klass->get_next_part = (gchar* (*)(GopXMLSource *self))
				gop_xml_source_gtkliststore_get_next_part;

	klass->rewind = (void (*)(GopXMLSource *self))
			 gop_xml_source_gtkliststore_rewind;
}

static void
instance_init (GopXMLSourceGtkListStore *self)
{
	self->priv = g_new0 (GopXMLSourceGtkListStorePriv, 1);
}

static void
instance_dispose (GObject *instance)
{
	GopXMLSourceGtkListStore *self = (GopXMLSourceGtkListStore*) instance;

	if (self->priv == NULL)
		return;

	if (self->priv->store) {
		g_object_unref (self->priv->store);
		self->priv->store = NULL;
	}

	free_strlist (self);

	g_free (self->priv);
	self->priv = NULL;

	gop_xml_source_gtkliststore_parent_class->dispose (G_OBJECT (self));
}

static void
class_init (GopXMLSourceGtkListStoreClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gop_xml_source_gtkliststore_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);
}

GType
gop_xml_source_gtkliststore_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GopXMLSourceGtkListStoreClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GopXMLSourceGtkListStore),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo xml_source_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GopXMLSourceGtkListStore", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GOP_TYPE_XML_SOURCE, &xml_source_info);
        }
        return type;
}

/**
 * gop_xml_source_gtkliststore_new: 
 * 
 * Creates an XML serialiser instance for writing GtkListStore objects. Internal use only.
 * 
 * Returns: GtkListStore XML serialiser instance.
 */
GopXMLSource* 
gop_xml_source_gtkliststore_new (void)
{
	return (GopXMLSource*) g_object_new (GOP_TYPE_XML_SOURCE_GTKLISTSTORE, NULL);
}

static gboolean    
liststore_foreach_func (GtkTreeModel *model,
			GtkTreePath *path,
			GtkTreeIter *iter,
			GSList **list)
{
	GValue val = {0}; 
	GValue strval = {0};
	gchar *str = NULL;
	gchar *pathstr = NULL;
	gint n_cols = gtk_tree_model_get_n_columns (model);
	gint i;

	*list = g_slist_prepend (*list, g_strdup ("</row>"));

	for (i = n_cols - 1; i >= 0; i--) {

		g_value_init (&strval, G_TYPE_STRING);

		/* g_value_init (val, gtk_tree_model_get_column_type (model, i)); */
		gtk_tree_model_get_value (model, iter, i, &val);

		g_value_transform (&val, &strval);
		str = g_strdup_printf ("<v c=\"%d\">%s</v>", i, g_value_get_string (&strval));
		*list = g_slist_prepend (*list, str);

		g_value_unset (&val);
		g_value_unset (&strval);
	}

	pathstr = gtk_tree_path_to_string (path);
	*list = g_slist_prepend (*list, g_strdup_printf ("<row n=\"%s\">", pathstr));
	g_free (pathstr);

	return FALSE;	
}

void
gop_xml_source_gtkliststore_set_object (GopXMLSourceGtkListStore *self, 
					gchar const              *id, 
					GtkListStore             *store)
{
	GType type;
	const gchar *type_name = NULL;
	gchar *str = NULL;
	gchar *pos = NULL;
	gsize size;
	gint n_cols;
	gint i;


	if (self->priv->store) {
		g_object_unref (self->priv->store);
		self->priv->store = NULL;
	}

	free_strlist (self);

	self->priv->store = store;
	g_object_ref (self->priv->store);


	self->priv->strlist = g_slist_prepend (self->priv->strlist, g_strdup ("</o>"));

	/* data */
	gtk_tree_model_foreach (GTK_TREE_MODEL (store), 
				(GtkTreeModelForeachFunc) liststore_foreach_func,
                                &self->priv->strlist);	

	/* column definitions */
	n_cols = gtk_tree_model_get_n_columns (GTK_TREE_MODEL (store));
	for (i = 0; i < n_cols; i++) {
		type = gtk_tree_model_get_column_type (GTK_TREE_MODEL (store), i);
		type_name = g_type_name (type);
		self->priv->strlist = g_slist_prepend (self->priv->strlist, 
				g_strdup_printf ("<c n=\"%d\" t=\"%s\" />\n", i, type_name));
	}

	self->priv->strlist = g_slist_prepend (self->priv->strlist, 
					       g_strdup_printf ("<o n=\"%s\" t=\"%s\">", 
								id,
								G_OBJECT_TYPE_NAME (store)));


	gop_xml_source_gtkliststore_rewind (self);
}

gchar*
gop_xml_source_gtkliststore_get_next_part (GopXMLSourceGtkListStore *self)
{
	gchar *ret = NULL;

	if (self->priv->item) {
		ret = g_strdup ((gchar*) self->priv->item->data);
		self->priv->item = self->priv->item->next;
	}
	
	return ret;
}

void 
gop_xml_source_gtkliststore_rewind (GopXMLSourceGtkListStore *self)
{
	self->priv->item = self->priv->strlist;
}
