/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gopersist/gop-xml-sink-internal.h>
#include "gop-xml-sink-gtkliststore.h"

/* GopXMLSink interface */
void 
gop_xml_sink_gtkliststore_start_element (GopXMLSinkGtkListStore *self,
					 const gchar       *tag,
					 const gchar      **attr_names,
					 const gchar      **attr_values,
					 GError           **error);
void 
gop_xml_sink_gtkliststore_end_element (GopXMLSinkGtkListStore *self,
				       const gchar       *tag,
				       GError           **error);
void 
gop_xml_sink_gtkliststore_text (GopXMLSinkGtkListStore *self,
				const gchar       *text,
				gsize              text_len,  
				GError           **error);
GObject* gop_xml_sink_gtkliststore_get_object (GopXMLSinkGtkListStore *self);
gchar* gop_xml_sink_gtkliststore_get_object_id (GopXMLSinkGtkListStore *self);

struct _GopXMLSinkGtkListStorePriv {
	/* object level */
	gchar  *object_id;
	GType *col_types;
	GValue *col_values;
	gint *col_indices;
	gint n_cols;
	/* row level */
	gint n_row;
	/* value level */
	gint n_col;
	/* instantiated object */
	GtkListStore *store;
};

static GObjectClass *gop_xml_sink_gtkliststore_parent_class = NULL;

static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GopXMLSinkClass *klass = (GopXMLSinkClass *) iface;

	klass->start_element = (void (*)(GopXMLSink   *self,
					 const gchar  *tag,
					 const gchar **attr_names,
					 const gchar **attr_values,
					 GError      **error))
			       gop_xml_sink_gtkliststore_start_element;

	klass->end_element = (void (*)(GopXMLSink   *self,
				       const gchar  *tag,
				       GError      **error))
			     gop_xml_sink_gtkliststore_end_element;

	klass->text = (void (*)(GopXMLSink   *self,
				const gchar  *text,
				gsize         text_len,  
				GError      **error))
		      gop_xml_sink_gtkliststore_text;
	
	klass->get_object = (GObject* (*)(GopXMLSink *self))
			    gop_xml_sink_gtkliststore_get_object;

	klass->get_object_id = (gchar* (*)(GopXMLSink *self))
			       gop_xml_sink_gtkliststore_get_object_id;
}

static void
instance_init (GopXMLSinkGtkListStore *self)
{
	self->priv = g_new0 (GopXMLSinkGtkListStorePriv, 1);

	self->priv->n_cols = 0;
	self->priv->n_row = -1;
	self->priv->n_col = -1;
}

static void
instance_dispose (GObject *instance)
{
	GopXMLSinkGtkListStore *self = (GopXMLSinkGtkListStore*) instance;

	if (self->priv == NULL)
		return;

	if (self->priv->store) {
		g_object_unref (self->priv->store);
		self->priv->store = NULL;
	}

	if (self->priv->object_id) {
		g_free (self->priv->object_id);
		self->priv->object_id = NULL;
	}

	g_free (self->priv);
	self->priv = NULL;

	gop_xml_sink_gtkliststore_parent_class->dispose (G_OBJECT (self));
}

static void
class_init (GopXMLSinkGtkListStoreClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gop_xml_sink_gtkliststore_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);
}

GType
gop_xml_sink_gtkliststore_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GopXMLSinkGtkListStoreClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GopXMLSinkGtkListStore),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo xml_sink_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GopXMLSinkGtkListStore", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GOP_TYPE_XML_SINK, &xml_sink_info);
        }
        return type;
}

/**
 * gop_xml_sink_gtkliststore_new: 
 * 
 * Creates an XML accumulator instance for reading GtkListStore objects. Internal use only.
 * 
 * Returns: GtkListStore XML accumulator instance.
 */
GopXMLSink* 
gop_xml_sink_gtkliststore_new (void)
{
	return (GopXMLSink*) g_object_new (GOP_TYPE_XML_SINK_GTKLISTSTORE, NULL);
}

void 
gop_xml_sink_gtkliststore_start_element (GopXMLSinkGtkListStore *self,
					 const gchar            *tag,
					 const gchar           **attr_names,
					 const gchar           **attr_values,
					 GError                **error)
{
	if (*tag == 'o') {

		/* clean up from prev object */
		if (self->priv->object_id) {
			g_free (self->priv->object_id);
			self->priv->object_id = NULL;
		}
		if (self->priv->store) {
			g_object_unref (self->priv->store);
			self->priv->store = NULL;
		}

		/* object root */
		self->priv->object_id = g_strdup (attr_values[0]);
	}
	else if (*tag == 'c') {
		/* column def */
		gint n_col = atoi (attr_values[0]);
		if (self->priv->col_types == NULL) {
			self->priv->n_cols = n_col + 1;
			self->priv->col_types = g_new0 (GType, self->priv->n_cols);
			self->priv->col_values = g_new0 (GValue, self->priv->n_cols);
			self->priv->col_indices = g_new0 (gint, self->priv->n_cols);
		}
		self->priv->col_types[n_col] = g_type_from_name (attr_values[1]);
		self->priv->col_indices[n_col] = atoi (attr_values[0]);
	}
	else if (0 == strcmp (tag, "row")) {
		/* row */

		if (self->priv->store == NULL) {

			GType *col_types = g_new0 (GType, self->priv->n_cols);
			gint idx;
			gint i;

			for (i = 0; i < self->priv->n_cols; i++) {
				idx = self->priv->col_indices[i];
				col_types[i] = self->priv->col_types[idx];
			}

			self->priv->store = gtk_list_store_newv (self->priv->n_cols, 
								 col_types);
			g_object_ref (self->priv->store);
			/* TODO g_object_sink (self->priv->store); */

			g_free (col_types);
			col_types = NULL;
		}

		self->priv->n_row = atoi (attr_values[0]);
	}
	else if (*tag == 'v') {
		/* value */
		self->priv->n_col = atoi (attr_values[0]);
	}
	else {
		g_warning ("%s: ignoring unknown tag '%s'", __FUNCTION__, tag);
	}
}

void 
gop_xml_sink_gtkliststore_end_element (GopXMLSinkGtkListStore *self,
				       const gchar            *tag,
				       GError                **error)
{
	if (*tag == 'o') {

		gop_xml_sink_emit_done (GOP_XML_SINK (self));

		/* clean up */
		self->priv->n_cols = -1;
		self->priv->n_row = -1;
		self->priv->n_col = -1;
		if (self->priv->object_id) {
			g_free (self->priv->object_id);
			self->priv->object_id = NULL;
		}
		if (self->priv->col_types) {
			g_free (self->priv->col_types);
			self->priv->col_types = NULL;
		}
		if (self->priv->col_values ) {
			g_free (self->priv->col_values);
			self->priv->col_values = NULL;
		}
		if (self->priv->col_indices) {
			g_free (self->priv->col_indices);
			self->priv->col_indices = NULL;
		}
	}
	else if (*tag == 'c') {
		/* nothing to do */
	}
	else if (0 == strcmp (tag, "row")) {

		GtkTreeIter iter;
		gint i;

		gtk_list_store_insert_with_valuesv (self->priv->store,
						    &iter,
						    self->priv->n_row,
						    self->priv->col_indices,
						    self->priv->col_values,
						    self->priv->n_cols);

		/* clean up */
		for (i = 0; i < self->priv->n_cols; i++) {
			g_value_unset (&self->priv->col_values[i]);
		}
	}
	else if (*tag == 'v') {
		/* value */
		self->priv->n_col = -1;
	}
	else {
		g_warning ("%s: ignoring unknown tag '%s'", __FUNCTION__, tag);
	}

}

void 
gop_xml_sink_gtkliststore_text (GopXMLSinkGtkListStore *self,
				const gchar            *text,
				gsize                   text_len,  
				GError                **error)
{
	if (self->priv->n_col > -1) {
		/* convert value */
		if (G_TYPE_STRING == self->priv->col_types[self->priv->n_col]) {
			/* no conversion needed */
			g_value_init (&self->priv->col_values[self->priv->n_col], 
				      G_TYPE_STRING);
			g_value_take_string (&self->priv->col_values[self->priv->n_col], 
					     g_strndup (text, text_len));
		}
		else {
			GValue strval = {0};
			g_value_init (&strval, G_TYPE_STRING);
			g_value_take_string (&strval, g_strndup (text, text_len));

			g_value_init (&self->priv->col_values[self->priv->n_col], 
				       self->priv->col_types[self->priv->n_col]);
			g_assert (g_value_type_transformable (G_TYPE_STRING, 
				  self->priv->col_types[self->priv->n_col]));

			g_value_transform (&strval, &self->priv->col_values[self->priv->n_col]);
			g_value_unset (&strval);
		}
	}
/*
	else if (text_len > 0) {
		g_warning ("%s: ignoring text because prop_name == NULL", __FUNCTION__);
	}
*/
}

GObject* 
gop_xml_sink_gtkliststore_get_object (GopXMLSinkGtkListStore *self)
{
	g_object_ref (G_OBJECT (self->priv->store));
	return G_OBJECT (self->priv->store);
}

gchar* 
gop_xml_sink_gtkliststore_get_object_id (GopXMLSinkGtkListStore *self)
{
	return g_strdup (self->priv->object_id);
}
