/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_XML_SINK_GTKLISTSTORE_H__
#define __GOP_XML_SINK_GTKLISTSTORE_H__

#include <glib.h>
#include <glib-object.h>
#include <gopersist/gop-xml-sink.h>

G_BEGIN_DECLS

#define GOP_TYPE_XML_SINK_GTKLISTSTORE                  (gop_xml_sink_gtkliststore_get_type ())
#define GOP_XML_SINK_GTKLISTSTORE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_XML_SINK_GTKLISTSTORE, GopXMLSinkGtkListStore))
#define GOP_XML_SINK_GTKLISTSTORE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_XML_SINK_GTKLISTSTORE, GopXMLSinkGtkListStoreClass))
#define GOP_IS_XML_SINK_GTKLISTSTORE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_XML_SINK_GTKLISTSTORE))
#define GOP_IS_XML_SINK_GTKLISTSTORE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_XML_SINK_GTKLISTSTORE))
#define GOP_XML_SINK_GTKLISTSTORE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GOP_TYPE_XML_SINK_GTKLISTSTORE, GopXMLSinkGtkListStoreClass))

typedef struct _GopXMLSinkGtkListStore GopXMLSinkGtkListStore;
typedef struct _GopXMLSinkGtkListStorePriv GopXMLSinkGtkListStorePriv;
typedef struct _GopXMLSinkGtkListStoreClass GopXMLSinkGtkListStoreClass;

struct _GopXMLSinkGtkListStore {
	GObject parent;
	GopXMLSinkGtkListStorePriv *priv;
};

struct _GopXMLSinkGtkListStoreClass {
	GObjectClass parent;
};

GType gop_xml_sink_gtkliststore_get_type (void);

GopXMLSink* gop_xml_sink_gtkliststore_new (void);

G_END_DECLS

#endif /* __GOP_XML_SINK_GTKLISTSTORE_H__ */
