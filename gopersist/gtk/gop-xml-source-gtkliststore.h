/* GtkListStore persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_XML_SOURCE_GTKLISTSTORE_H__
#define __GOP_XML_SOURCE_GTKLISTSTORE_H__

#include <glib.h>
#include <glib-object.h>
#include <gopersist/gop-xml-source.h>

G_BEGIN_DECLS

#define GOP_TYPE_XML_SOURCE_GTKLISTSTORE                  (gop_xml_source_gtkliststore_get_type ())
#define GOP_XML_SOURCE_GTKLISTSTORE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_XML_SOURCE_GTKLISTSTORE, GopXMLSourceGtkListStore))
#define GOP_XML_SOURCE_GTKLISTSTORE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_XML_SOURCE_GTKLISTSTORE, GopXMLSourceGtkListStoreClass))
#define GOP_IS_XML_SOURCE_GTKLISTSTORE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_XML_SOURCE_GTKLISTSTORE))
#define GOP_IS_XML_SOURCE_GTKLISTSTORE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_XML_SOURCE_GTKLISTSTORE))
#define GOP_XML_SOURCE_GTKLISTSTORE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GOP_TYPE_XML_SOURCE_GTKLISTSTORE, GopXMLSourceGtkListStoreClass))

typedef struct _GopXMLSourceGtkListStore GopXMLSourceGtkListStore;
typedef struct _GopXMLSourceGtkListStorePriv GopXMLSourceGtkListStorePriv;
typedef struct _GopXMLSourceGtkListStoreClass GopXMLSourceGtkListStoreClass;

struct _GopXMLSourceGtkListStore {
	GObject parent;
	GopXMLSourceGtkListStorePriv *priv;
};

struct _GopXMLSourceGtkListStoreClass {
	GtkListStoreClass parent;
};

GType gop_xml_source_gtkliststore_get_type (void);

GopXMLSource* gop_xml_source_gtkliststore_new (void);

G_END_DECLS

#endif /* __GOP_XML_SOURCE_GTKLISTSTORE_H__ */
