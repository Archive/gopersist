
#ifndef __GOPERSIST_GTK_H__
#define __GOPERSIST_GTK_H__

#include <gopersist/gopersist.h>

void gopersist_gtk_init (int *argc, char **argv[]);
void gopersist_gtk_shutdown (void);

#endif /* __GOPERSIST_GTK_H__ */
