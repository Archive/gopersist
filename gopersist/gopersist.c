/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <glib-object.h>
#include "gop-transformers.h"
#include "gop-xml-sink-gobject.h"
#include "gop-xml-sink-registry.h"
#include "gop-xml-source-gobject.h"
#include "gop-xml-source-registry.h"

static gboolean gop_is_initialized = FALSE;

/**
 * gopersist_init: 
 * 
 * Initialise the gopersist library. Mandatory before calling any other 
 * functions.
 */ 
void 
gopersist_init (void)
{
	GopXMLSink *default_sink = NULL; 
	GopXMLSource *default_source = NULL;

	if (gop_is_initialized) {
		return;
	}
	
	g_type_init ();

	default_sink = gop_xml_sink_gobject_new ();
	gop_xml_sink_registry_init (default_sink);
	g_object_unref (default_sink);
	default_sink = NULL;

	default_source = gop_xml_source_gobject_new ();
	gop_xml_source_registry_init (default_source);
	g_object_unref (default_source);
	default_source = NULL;

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_BOOLEAN,
					 gop_transform__gchararray_gboolean);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_CHAR,
					 gop_transform__gchararray_gchar);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_DOUBLE,
					 gop_transform__gchararray_gdouble);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_FLOAT,
					 gop_transform__gchararray_gfloat);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_INT,
					 gop_transform__gchararray_gint);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_INT64,
					 gop_transform__gchararray_gint64);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_LONG,
					 gop_transform__gchararray_glong);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_UCHAR,
					 gop_transform__gchararray_guchar);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_UINT,
					 gop_transform__gchararray_guint);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_UINT64,
					 gop_transform__gchararray_guint64);

	g_value_register_transform_func (G_TYPE_STRING, 
					 G_TYPE_ULONG,
					 gop_transform__gchararray_gulong);

	gop_is_initialized = TRUE;
}

/**
 * gopersist_shutdown:
 * 
 * Shutdown the gopersist library.
 */
void 
gopersist_shutdown (void)
{
	gop_xml_sink_registry_shutdown ();
	gop_xml_source_registry_shutdown ();

	/* seems there is no way to unregister transform funcs (yet?) */
}
