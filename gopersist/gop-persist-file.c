/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gstdio.h>
#include "gop-persist-file.h"
#include "gop-persist.h"
#include "gop-xml-sink.h"
#include "gop-xml-sink-registry.h"
#include "gop-xml-source.h"
#include "gop-xml-source-registry.h"

/* GopPersist interface */
void gop_persist_file_load (GopPersistFile *self);
void gop_persist_file_register_object (GopPersistFile *self, gchar const *key, GObject *object);
void gop_persist_file_unregister_object (GopPersistFile *self, gchar const *key);
GObject* gop_persist_file_get_object (GopPersistFile *self, gchar const *key);
void gop_persist_file_save (GopPersistFile *self);

struct _GopPersistFilePriv {
	gchar *file_name;
	GHashTable *object_hash;
	/* parser status - file level */
	gboolean is_parsing;
	GSList *sink_list;
};

/* property IDs */
enum {
	_PROP_0,	
	PROP_FILE_NAME,
	_NUM_PROPS
};

static GObjectClass *gop_persist_file_parent_class = NULL;


static void
object_hash_destroy_key (gpointer data)
{
	g_free (data);
}

static void
object_hash_destroy_val (gpointer data)
{
	g_object_unref (G_OBJECT (data));
}


static void
interface_init (gpointer iface,
		gpointer iface_data)
{
	GopPersistClass *klass = (GopPersistClass *) iface;

	klass->register_object = (void (*)(GopPersist *self, gchar const *key, GObject *object))
				 gop_persist_file_register_object;
	klass->unregister_object = (void (*)(GopPersist *self, gchar const *key))
				   gop_persist_unregister_object;
	klass->get_object = (GObject* (*)(GopPersist *self, gchar const *key))
			    gop_persist_file_get_object;
	klass->load = (void (*)(GopPersist *self))
		       gop_persist_file_load;
	klass->save = (void (*)(GopPersist *self))
		       gop_persist_file_save;
}

static void
instance_init (GopPersistFile *self)
{
	self->priv = g_new0 (GopPersistFilePriv, 1);
	self->priv->object_hash = g_hash_table_new_full (g_str_hash, 
							 g_str_equal, 
							 object_hash_destroy_key, 
							 object_hash_destroy_val);
}

static void
instance_dispose (GObject *instance)
{
	GopPersistFile *self = (GopPersistFile*) instance;

	if (self->priv == NULL)
		return;

	if (self->priv->file_name) {
		g_free (self->priv->file_name);
		self->priv->file_name = NULL;
	}

	g_free (self->priv);
	self->priv = NULL;

	gop_persist_file_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *instance,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	GopPersistFile *self = GOP_PERSIST_FILE (instance);

	switch (prop_id) {
	case PROP_FILE_NAME:
		/* can only be set at construction time */
		g_assert (self->priv->file_name == NULL);
		self->priv->file_name = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (instance, prop_id, pspec);
	}
}

static void
get_property (GObject    *instance,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	GopPersistFile *self = GOP_PERSIST_FILE (instance);

	switch (prop_id) {
	case PROP_FILE_NAME:
		g_value_set_string (value, self->priv->file_name);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (instance, prop_id, pspec);
	}
}

static void
class_init (GopPersistFileClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	gop_persist_file_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_FILE_NAME,
		g_param_spec_string ("file-name", "Name of file",
			"File to serialize data to", NULL,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));
}

GType
gop_persist_file_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GopPersistFileClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (GopPersistFile),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
		static const GInterfaceInfo persist_file_info = {
			(GInterfaceInitFunc) interface_init,	/* interface_init */
			NULL,					/* interface_finalize */
			NULL					/* interface_data */
		};
                type = g_type_register_static (G_TYPE_OBJECT, "GopPersistFile", &info, (GTypeFlags)0);
		g_type_add_interface_static (type, GOP_TYPE_PERSIST, &persist_file_info);
        }
        return type;
}

/**
 * gop_persist_file_new:
 * @file_name: Location of file to use as serialisation media.
 * 
 * Instantiate object that handles persistence using files.
 * 
 * Returns: GopPersist instance.
 */
GopPersist* 
gop_persist_file_new (gchar const *file_name)
{
	return (GopPersist*) g_object_new (GOP_TYPE_PERSIST_FILE, 
					   "file-name", file_name, 
					   NULL);
}

void 
gop_persist_file_register_object (GopPersistFile *self, 
				  gchar const *key, 
				  GObject *object)
{
	g_assert (key && *key);

	/* TODO check if key/object already in use? */

	g_hash_table_insert (self->priv->object_hash, 
			     g_strdup (key), 
			     g_object_ref (object));
}

void 
gop_persist_file_unregister_object (GopPersistFile *self, 
				    gchar const *key)
{
	g_hash_table_remove (self->priv->object_hash, key);
}

GObject* 
gop_persist_file_get_object (GopPersistFile *self, 
			     gchar const *key)
{
	GObject *ret = NULL;

	ret = (GObject*) g_hash_table_lookup (self->priv->object_hash, key);
	g_object_ref (ret);

	return ret;
}

static void 
object_hash_foreach_serialize (gchar const *key,
			       GObject *object,
			       GSList **list)
{
	GSList *item = NULL;
	GSList *last_item = NULL;
	GopXMLSource *source = NULL;
	gchar *part = NULL;

	source = gop_xml_source_registry_query (G_OBJECT_TYPE_NAME (object));
	g_assert (source);
	gop_xml_source_set_object (source, key, object);

	last_item = g_slist_last (*list);
	while (NULL != (part = gop_xml_source_get_next_part (source))) {
		item = g_slist_append (NULL, part);
		if (last_item)
			last_item->next = item;
		else
			*list = item;
		last_item = item;
	}
}

void 
gop_persist_file_save (GopPersistFile *self)
{
	FILE *ostream = NULL; 
	GSList *items = NULL;
	GSList *item = NULL;

	g_assert (self->priv->file_name);
	ostream = fopen (self->priv->file_name, "w");
	g_assert (ostream);


	fprintf (ostream, "<?xml version=\"1.0\" ?>");
	fprintf (ostream, "<gopersist version=\"1.0\">");

	g_hash_table_foreach (self->priv->object_hash, 
			      (GHFunc) object_hash_foreach_serialize, 
			      &items);

	item = items;
	while (item != NULL) {

		fprintf (ostream, "%s", (gchar*) item->data);
		g_free (item->data);
		item = item->next;
	}
	item = NULL;
	g_slist_free (items);
	item = NULL;

	fprintf (ostream, "</gopersist>");
	fclose (ostream);
}

static void
object_done_cb (GopPersistFile *self, 
		GopXMLSink     *sink)
{
	GObject *object = NULL;
	gchar *object_id = NULL;
	GSList *item = NULL;

	/* store object */
	object = gop_xml_sink_get_object (sink);
	object_id = gop_xml_sink_get_object_id (sink);
	g_hash_table_insert (self->priv->object_hash, object_id, object);
	g_object_ref (object);

	/* pop sink list, continue with parent */
	item = self->priv->sink_list;
	self->priv->sink_list = self->priv->sink_list->next;
	g_object_unref (G_OBJECT (item->data));
	g_slist_free_1 (item);
	item = NULL;
}

static void 
start_element (GMarkupParseContext *context,
	       const gchar         *tag,
	       const gchar        **attr_names,
	       const gchar        **attr_values,
	       gpointer             data,
	       GError             **error)
{
	GopPersistFile *self = GOP_PERSIST_FILE (data);
	GopXMLSink *sink = NULL;

	if (0 == strcmp (tag, "gopersist")) {

		self->priv->is_parsing = TRUE;
		return;
	}
	else if (*tag == 'o') {

		sink = gop_xml_sink_registry_query (attr_values[1]);
		g_assert (sink);
		g_object_ref (sink);
		g_signal_connect_swapped (sink, "done", G_CALLBACK (object_done_cb), self);
		self->priv->sink_list = g_slist_prepend (self->priv->sink_list, sink);
	}

	gop_xml_sink_start_element (GOP_XML_SINK (self->priv->sink_list->data), 
				    tag, attr_names, attr_values, error);
}

static void 
end_element (GMarkupParseContext *context,
	     const gchar         *tag,
	     gpointer             data,
	     GError             **error)
{
	GopPersistFile *self = GOP_PERSIST_FILE (data);

	if (0 == strcmp (tag, "gopersist")) {

		self->priv->is_parsing = FALSE;
		return;
	}
	/*
	else if (*tag == 'o') {
	
	   hmm, maybe we should force pop the sink list here but let's try 
	   relying on the signalling mechanism first
	}
	*/

	gop_xml_sink_end_element (GOP_XML_SINK (self->priv->sink_list->data), 
				  tag, error);
}

static void 
text (GMarkupParseContext *context,
      const gchar         *text,
      gsize                text_len,  
      gpointer             data,
      GError             **error)
{
	GopPersistFile *self = GOP_PERSIST_FILE (data);

	if (self->priv->is_parsing && 
	    self->priv->sink_list) {
		gop_xml_sink_text (GOP_XML_SINK (self->priv->sink_list->data), 
				   text, text_len, error);
	}
}

static void 
passthrough (GMarkupParseContext *context,
	     const gchar         *passthrough_text,
	     gsize                text_len,  
	     gpointer             data,
	     GError             **error)
{
	/* nothing to do */
}

static void 
error (GMarkupParseContext *context,
       GError              *error,
       gpointer             data)
{
	GopPersistFile *self = GOP_PERSIST_FILE (data);

	g_error (error->message);
}

void 
gop_persist_file_load (GopPersistFile *self)
{
	GMarkupParseContext *context = NULL;
	GMarkupParser parser;
	GError *err = NULL;
	gchar *xml_string = NULL;
	gsize xml_len;
	gboolean ret;

	parser.start_element = start_element;
	parser.end_element = end_element;
	parser.text = text;
	parser.passthrough = passthrough;
	parser.error = error;

	ret = g_file_get_contents (self->priv->file_name, &xml_string, 
				   &xml_len, &err);
	if (!ret) {
		g_error (err->message);
	}

	context = g_markup_parse_context_new (&parser, 
					      (GMarkupParseFlags)0, 
					      self, 
					      (GDestroyNotify)NULL);

	ret = g_markup_parse_context_parse (context, xml_string, xml_len, &err);
	if (!ret) {
		g_error (err->message);
	}

	if (xml_string) {
		g_free (xml_string);
		xml_string = NULL;
	}
}
