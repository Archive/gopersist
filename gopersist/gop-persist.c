/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gop-persist.h"

static void
base_init (gpointer iface)
{
	static gboolean initialized = FALSE;

	if (initialized) {
		return;
	}

	/* create interface signals here. */

	initialized = TRUE;
}

GType
gop_persist_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GopPersistClass),
			base_init,   /* base_init */
			NULL,   /* base_finalize */
			NULL,   /* class_init */
			NULL,   /* class_finalize */
			NULL,   /* class_data */
			0,
			0,      /* n_preallocs */
			NULL    /* instance_init */
		};
		type = g_type_register_static (G_TYPE_INTERFACE, "GopPersist", 
					       &info, (GTypeFlags)0);
	}
	return type;
}

/**
 * gop_persist_register_object:
 * @self: Self instance.
 * @key: Unique string that identifies the object, e.g. for later retrieval.
 * @object: The object that should be persisted.
 * 
 * Register an object for serialisation.
 */
void 
gop_persist_register_object (GopPersist *self, 
			     gchar const *key, 
			     GObject *object)
{
	return GOP_PERSIST_GET_CLASS (self)->register_object (self, key, object);
}

/**
 * gop_persist_unregister_object:
 * @self: Self instance.
 * @key: Unique string that identifies the object.
 * 
 * Remove an object instance so it will not be serialised.
 */
void 
gop_persist_unregister_object (GopPersist *self, 
			       gchar const *key)
{
	return GOP_PERSIST_GET_CLASS (self)->unregister_object (self, key);
}

/**
 * gop_persist_get_object:
 * @self: Self instance.
 * @key: Unique string that identifies the object.
 * 
 * Retrieve an object that has been deserialised by gopersist.
 * 
 * Returns: The object instance or <literal>NULL</literal> if the key is unknown. 
 * <literal>g_object_unref()</literal> must be called on the returned object when
 * it's no longer referenced.
 */
GObject* 
gop_persist_get_object (GopPersist *self, 
			gchar const *key)
{
	return GOP_PERSIST_GET_CLASS (self)->get_object (self, key);
}

/**
 * gop_persist_load:
 * @self: Self instance.
 *
 * Loads and instantiates all objects from the serialisation media specified at
 * construction time.
 */
void 
gop_persist_load (GopPersist *self)
{
	return GOP_PERSIST_GET_CLASS (self)->load (self);
}

/**
 * gop_persist_save:
 * @self: Self instance.
 *
 * Saves all objects to the serialisation media specified at construction
 * time.
 */
void 
gop_persist_save (GopPersist *self)
{
	return GOP_PERSIST_GET_CLASS (self)->save (self);
}
