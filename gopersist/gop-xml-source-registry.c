/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gop-xml-source-registry.h"

static GHashTable* gop_xml_source_hash = NULL;
#define GOP_XML_SOURCE_DEFAULT "-gop-xml-source-default-"

static void
source_hash_destroy_val (gpointer data)
{
	g_object_unref (G_OBJECT (data));
}

/**
 * gop_xml_source_registry_init: 
 * @default_source: Fallback serialiser for generic GObjects.
 * 
 * Initialise xml serialiser subsystem. Called by <literal>gopersist_init()</literal>, 
 * no need to call as gopersist consumer.
 */
void 
gop_xml_source_registry_init (GopXMLSource *default_source)
{
	g_assert (!gop_xml_source_hash);

	gop_xml_source_hash = g_hash_table_new_full (g_str_hash, 
						     g_str_equal, 
						     NULL, 
						     source_hash_destroy_val);

	g_hash_table_insert (gop_xml_source_hash, (gchar*) GOP_XML_SOURCE_DEFAULT, default_source);
	g_object_ref (default_source);
}

/**
 * gop_xml_source_registry_register:
 * @type_name: Name of handled type.
 * @source: Serialiser implementation.
 * 
 * Register a GObject/XML serialiser for a certain GType.
 */
void 
gop_xml_source_registry_register (gchar const *type_name, 
				  GopXMLSource *source)
{
	g_assert (gop_xml_source_hash);
	g_hash_table_insert (gop_xml_source_hash, (gpointer) type_name, (gpointer) source);	
	g_object_ref (source);
}

/**
 * gop_xml_source_registry_query:
 * @type_name: Name of type to get sink for.
 * 
 * Get serialiser for a certain type. If no specific one is available the default
 * one is returned.
 * 
 * Returns: Serialiser instance, owned by the sink registry. It is not allowed to call 
 * <literal>g_object_unref()</literal> on it.
 */
GopXMLSource* 
gop_xml_source_registry_query (gchar const *type_name)
{
	GObject *obj = NULL;

	obj = (GObject*) g_hash_table_lookup (gop_xml_source_hash, type_name);
	if (!obj) {
		obj = (GObject*) g_hash_table_lookup (gop_xml_source_hash, GOP_XML_SOURCE_DEFAULT);
	}

	return GOP_XML_SOURCE (obj);
}

/**
 * gop_xml_source_registry_shutdown:
 *
 * Shutdown xml serialiser subsystem. Called by <literal>gopersist_shutdown()</literal>, 
 * no need to call as gopersist consumer.
 */
void 
gop_xml_source_registry_shutdown (void)
{
	g_assert (gop_xml_source_hash);
	g_hash_table_destroy (gop_xml_source_hash);
	gop_xml_source_hash = NULL;
}
