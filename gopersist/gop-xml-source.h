/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_XML_SOURCE_H__
#define __GOP_XML_SOURCE_H__

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define GOP_TYPE_XML_SOURCE                  (gop_xml_source_get_type ())
#define GOP_XML_SOURCE(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_XML_SOURCE, GopXMLSource))
#define GOP_XML_SOURCE_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_XML_SOURCE, GopXMLSourceClass))
#define GOP_IS_XML_SOURCE(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_XML_SOURCE))
#define GOP_IS_XML_SOURCE_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_XML_SOURCE))
#define GOP_XML_SOURCE_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GOP_TYPE_XML_SOURCE, GopXMLSourceClass))

typedef struct _GopXMLSource GopXMLSource;
typedef struct _GopXMLSourceClass GopXMLSourceClass;

struct _GopXMLSource {
	GObject parent;
};

struct _GopXMLSourceClass {
	GTypeInterface parent;
	/* vfuncs */
	void (*set_object) (GopXMLSource *self, gchar const *id, GObject *object);
	gchar* (*get_next_part) (GopXMLSource *self);
	void (*rewind) (GopXMLSource *self);
};

GType gop_xml_source_get_type (void);

void gop_xml_source_set_object (GopXMLSource *self, gchar const *id, GObject *object);
gchar *gop_xml_source_get_next_part (GopXMLSource *self);
void gop_xml_source_rewind (GopXMLSource *self);

G_END_DECLS

#endif /* __GOP_XML_SOURCE_H__ */
