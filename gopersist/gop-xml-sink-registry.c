/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gop-xml-sink-registry.h"

static GHashTable* gop_xml_sink_hash = NULL;
#define GOP_XML_SINK_DEFAULT "-gop-xml-sink-default-"

static void
sink_hash_destroy_val (gpointer data)
{
	g_object_unref (G_OBJECT (data));
}

/**
 * gop_xml_sink_registry_init: 
 * @default_sink: Fallback deserialiser for generic GObjects.
 * 
 * Initialise xml deserialiser subsystem. Called by <literal>gopersist_init()</literal>, 
 * no need to call as gopersist consumer.
 */
void 
gop_xml_sink_registry_init (GopXMLSink *default_sink)
{
	g_assert (!gop_xml_sink_hash);

	gop_xml_sink_hash = g_hash_table_new_full (g_str_hash, 
						   g_str_equal, 
						   NULL, 
						   sink_hash_destroy_val);

	g_hash_table_insert (gop_xml_sink_hash, (gchar*) GOP_XML_SINK_DEFAULT, default_sink);
	g_object_ref (default_sink);
}

/**
 * gop_xml_sink_registry_register:
 * @type_name: Name of handled type.
 * @sink: Deserialiser implementation.
 * 
 * Register a GObject/XML deserialiser for a certain GType.
 */
void 
gop_xml_sink_registry_register (gchar const *type_name, 
				GopXMLSink *sink)
{
	g_assert (gop_xml_sink_hash);
	g_hash_table_insert (gop_xml_sink_hash, (gpointer) type_name, (gpointer) sink);	
	g_object_ref (sink);
}

/**
 * gop_xml_sink_registry_query:
 * @type_name: Name of type to get sink for.
 * 
 * Get deserialiser for a certain type. If no specific one is available the default
 * one is returned.
 * 
 * Returns: Deserialiser instance, owned by the sink registry. It is not allowed to call 
 * <literal>g_object_unref()</literal> on it.
 */
GopXMLSink* 
gop_xml_sink_registry_query (gchar const *type_name)
{
	GObject *obj = NULL;

	obj = (GObject*) g_hash_table_lookup (gop_xml_sink_hash, type_name);
	if (!obj) {
		obj = (GObject*) g_hash_table_lookup (gop_xml_sink_hash, GOP_XML_SINK_DEFAULT);
	}

	return GOP_XML_SINK (obj);
}

/**
 * gop_xml_sink_registry_shutdown:
 *
 * Shutdown xml deserialiser subsystem. Called by <literal>gopersist_shutdown()</literal>, 
 * no need to call as gopersist consumer.
 */
void 
gop_xml_sink_registry_shutdown (void)
{
	g_assert (gop_xml_sink_hash);
	g_hash_table_destroy (gop_xml_sink_hash);
	gop_xml_sink_hash = NULL;
}
