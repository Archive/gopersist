/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gop-xml-source.h"

static void
base_init (gpointer iface)
{
	static gboolean initialized = FALSE;

	if (initialized) {
		return;
	}

	initialized = TRUE;
}

GType
gop_xml_source_get_type (void)
{
	static GType type = 0;
	if (!type) {
		static const GTypeInfo info = {
			sizeof (GopXMLSourceClass),
			base_init,   /* base_init */
			NULL,   /* base_finalize */
			NULL,   /* class_init */
			NULL,   /* class_finalize */
			NULL,   /* class_data */
			0,
			0,      /* n_preallocs */
			NULL    /* instance_init */
		};
		type = g_type_register_static (G_TYPE_INTERFACE, "GopXMLSource", 
					       &info, (GTypeFlags)0);
	}
	return type;
}

/**
 * gop_xml_source_set_object:
 * @self: Self instance.
 * @id: Unique object ID.
 * @object: Object to register with XML serialiser subsystem.
 * 
 * Register an object for serialisation.
 */
void
gop_xml_source_set_object (GopXMLSource *self, 
			   gchar const *id, 
			   GObject *object)
{
	GOP_XML_SOURCE_GET_CLASS (self)->set_object (self, id, object);
}

/**
 * gop_xml_source_get_next_part:
 * @self: Self instance.
 *
 * Get next string part of the serialised object. 
 * 
 * Returns: String part, <literal>NULL</literal> after last part.
 */
gchar*
gop_xml_source_get_next_part (GopXMLSource *self)
{
	return GOP_XML_SOURCE_GET_CLASS (self)->get_next_part (self);
}

/** 
 * gop_xml_source_rewind:
 * @self: Self instance.
 * 
 * Internal rewind of string iterator.
 */
void 
gop_xml_source_rewind (GopXMLSource *self)
{
	GOP_XML_SOURCE_GET_CLASS (self)->rewind (self);
}
