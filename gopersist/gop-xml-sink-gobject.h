/* GObject persistence library
 * Copyright (C) 2005 Robert Staudinger <robert.staudinger@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GOP_XML_SINK_GOBJECT_H__
#define __GOP_XML_SINK_GOBJECT_H__

#include <glib.h>
#include <glib-object.h>
#include <gopersist/gop-xml-sink.h>

G_BEGIN_DECLS

#define GOP_TYPE_XML_SINK_GOBJECT                  (gop_xml_sink_gobject_get_type ())
#define GOP_XML_SINK_GOBJECT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GOP_TYPE_XML_SINK_GOBJECT, GopXMLSinkGObject))
#define GOP_XML_SINK_GOBJECT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), GOP_TYPE_XML_SINK_GOBJECT, GopXMLSinkGObjectClass))
#define GOP_IS_XML_SINK_GOBJECT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GOP_TYPE_XML_SINK_GOBJECT))
#define GOP_IS_XML_SINK_GOBJECT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), GOP_TYPE_XML_SINK_GOBJECT))
#define GOP_XML_SINK_GOBJECT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), GOP_TYPE_XML_SINK_GOBJECT, GopXMLSinkGObjectClass))

typedef struct _GopXMLSinkGObject GopXMLSinkGObject;
typedef struct _GopXMLSinkGObjectPriv GopXMLSinkGObjectPriv;
typedef struct _GopXMLSinkGObjectClass GopXMLSinkGObjectClass;

struct _GopXMLSinkGObject {
	GObject parent;
	GopXMLSinkGObjectPriv *priv;
};

struct _GopXMLSinkGObjectClass {
	GObjectClass parent;
};

GType gop_xml_sink_gobject_get_type (void);

GopXMLSink* gop_xml_sink_gobject_new (void);

G_END_DECLS

#endif /* __GOP_XML_SINK_GOBJECT_H__ */
