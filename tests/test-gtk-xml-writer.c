
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gopersist/gtk/gopersist-gtk.h>

int
main (int argc, 
      char *argv[])
{
	GtkListStore *store = NULL;
	GopPersist *persist = NULL;
	GtkTreeIter iter;

	gopersist_gtk_init (&argc, &argv);

	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
	g_printf ("%d: %s\n", 
			G_OBJECT_TYPE (store), 
			G_OBJECT_TYPE_NAME (store));


	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter, 0, "foo", 1, 1, -1);

	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter, 0, "bar", 1, 2, -1);

	persist = gop_persist_file_new ("persist-gtk-in.xml");

	gop_persist_register_object (persist, "testobj", G_OBJECT (store));
	gop_persist_save (persist);

	g_object_unref (G_OBJECT (persist));
	g_object_unref (G_OBJECT (store));

	return 0;
}
