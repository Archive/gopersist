
#include <glib/gstdio.h>
#include <gopersist/gopersist.h>
#include "basic-object.h"

int
main (int argc, char *argv[])
{
	GObject *obj = NULL;
	GopPersist *persist = NULL;

	gopersist_init ();

	persist = gop_persist_file_new ("persist-in.xml");
	obj = (GObject*) g_object_new (TYPE_BASIC_OBJECT, NULL);

	gop_persist_register_object (persist, "testobj", G_OBJECT (obj));
	gop_persist_save (persist);


	do {
		gchar c; 
		guchar uc;

		g_object_get (obj, 
			      "char-property", &c, 
			      "uchar-property", &uc, 
			      NULL);
		g_printf ("char-prop: %d %c\n", c, c);
		g_printf ("uchar-prop: %d %c\n", uc, uc);

	} while (0);

	g_object_unref (G_OBJECT (persist));
	g_object_unref (obj);

	return 0;
}
