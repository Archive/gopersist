#!/bin/sh

rm *.xml
./test-gtk-xml-writer
./test-gtk-xml-reader
xmllint --format persist-gtk-in.xml > persist-gtk-in_.xml
xmllint --format persist-gtk-out.xml > persist-gtk-out_.xml
diff -u persist-gtk-in_.xml persist-gtk-out_.xml
