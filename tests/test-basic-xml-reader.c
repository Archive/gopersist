
#include <glib/gstdio.h>
#include <gopersist/gopersist.h>
#include "basic-object.h"

int
main (int argc, char *argv[])
{
	GObject *obj = NULL;
	GObject *obj2 = NULL;
	GopPersist *persist = NULL;
	GopPersist *persist_out = NULL;
	GType type;

	gopersist_init ();

	/* make sure type is registered */
	type = basic_object_get_type ();

	persist = gop_persist_file_new ("persist-in.xml");
	gop_persist_load (persist);
	obj = gop_persist_get_object (persist, "testobj");


	persist_out = gop_persist_file_new ("persist-out.xml");
	gop_persist_register_object (persist_out, "testobj", obj);
	/* register a second object
	obj2 = g_object_new (TYPE_BASIC_OBJECT, NULL);
	gop_persist_register_object (persist_out, "obj2", obj2);
	*/
	gop_persist_save (persist_out);
	

	g_object_unref (G_OBJECT (persist));
	g_object_unref (G_OBJECT (persist_out));
	if (obj)
		g_object_unref (obj);
	if (obj2)
		g_object_unref (obj2);

	return 0;
}
