
#ifndef __BASIC_OBJECT_H__
#define __BASIC_OBJECT_H__

#include <glib.h>
#include <glib-object.h>

#define TYPE_BASIC_OBJECT                  (basic_object_get_type ())
#define BASIC_OBJECT(obj)                  (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_BASIC_OBJECT, BasicObject))
#define BASIC_OBJECT_CLASS(klass)          (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_BASIC_OBJECT, BasicObjectClass))
#define IS_BASIC_OBJECT(obj)               (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_BASIC_OBJECT))
#define IS_BASIC_OBJECT_CLASS(klass)       (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_BASIC_OBJECT))
#define BASIC_OBJECT_GET_CLASS(obj)        (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_BASIC_OBJECT, BasicObjectClass))

typedef struct _BasicObject BasicObject;
typedef struct _BasicObjectClass BasicObjectClass;

GType basic_object_get_type (void);

#endif /* __BASIC_OBJECT_H__ */
