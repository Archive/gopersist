
#include "basic-object.h"

struct _BasicObject {
	GObject parent;
	gboolean is_disposed;
	
	gboolean booleanprop;
	gchar charprop;
	gdouble doubleprop;
	gfloat floatprop;
	gint intprop;
	gint64 int64prop;
	glong longprop;
	gchar *stringprop;
	guchar ucharprop;
	guint uintprop;
	guint64 uint64prop;
	gulong ulongprop;
	guint32 unicharprop;
};

struct _BasicObjectClass {
	GObjectClass parent;
};

enum {
	_PROP_0,	
	PROP_G_TYPE_BOOLEAN,
	PROP_G_TYPE_CHAR,
	PROP_G_TYPE_DOUBLE,
	PROP_G_TYPE_FLOAT,
	PROP_G_TYPE_INT,
	PROP_G_TYPE_INT64,
	PROP_G_TYPE_LONG,
	PROP_G_TYPE_STRING,
	PROP_G_TYPE_UCHAR,
	PROP_G_TYPE_UINT,
	PROP_G_TYPE_UINT64,
	PROP_G_TYPE_ULONG,
	PROP_G_TYPE_UNICHAR, /* doesn't exist (only for params, props), stored as guint32 */
	_NUM_PROPS
};

static GObjectClass *basic_object_parent_class = NULL;

static void
instance_init (BasicObject *self)
{
	self->is_disposed = FALSE;
	self->stringprop = NULL;
}

static void
instance_dispose (GObject *instance)
{
	BasicObject *self = BASIC_OBJECT (instance);

	if (self->is_disposed)
		return;

	self->is_disposed = TRUE;

	basic_object_parent_class->dispose (G_OBJECT (self));
}

static void
set_property (GObject      *instance,
	      guint         prop_id,
	      GValue const *value,
	      GParamSpec   *pspec)
{
	BasicObject *self = BASIC_OBJECT (instance);

	switch (prop_id) {
	case PROP_G_TYPE_BOOLEAN:
		self->booleanprop = g_value_get_boolean (value);
		break;
	case PROP_G_TYPE_CHAR:
		self->charprop = g_value_get_char (value);
		break;
	case PROP_G_TYPE_DOUBLE:
		self->doubleprop = g_value_get_double (value);
		break;
	case PROP_G_TYPE_FLOAT:
		self->floatprop = g_value_get_float (value);
		break;
	case PROP_G_TYPE_INT:
		self->intprop = g_value_get_int (value);
		break;
	case PROP_G_TYPE_INT64:
		self->int64prop = g_value_get_int64 (value);
		break;
	case PROP_G_TYPE_LONG:
		self->longprop = g_value_get_long (value);
		break;
	case PROP_G_TYPE_STRING:
		if (self->stringprop != NULL) {
			g_free (self->stringprop);
		}
		self->stringprop = g_value_dup_string (value);
		break;
	case PROP_G_TYPE_UCHAR:
		self->ucharprop = g_value_get_uchar (value);
		break;
	case PROP_G_TYPE_UINT:
		self->uintprop = g_value_get_uint (value); 
		break;
	case PROP_G_TYPE_UINT64:
		self->uint64prop = g_value_get_uint64 (value);
		break;
	case PROP_G_TYPE_ULONG:
		self->ulongprop = g_value_get_ulong (value);
		break;
	case PROP_G_TYPE_UNICHAR:
		self->unicharprop = g_value_get_uint (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (instance, prop_id, pspec);
	}
}

static void
get_property (GObject    *instance,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	BasicObject *self = BASIC_OBJECT (instance);

	switch (prop_id) {
	case PROP_G_TYPE_BOOLEAN:
		g_value_set_boolean (value, self->booleanprop);
		break;
	case PROP_G_TYPE_CHAR:
		g_value_set_char (value, self->charprop);
		break;
	case PROP_G_TYPE_DOUBLE:
		g_value_set_double (value, self->doubleprop);
		break;
	case PROP_G_TYPE_FLOAT:
		g_value_set_float (value, self->floatprop);
		break;
	case PROP_G_TYPE_INT:
		g_value_set_int (value, self->intprop);
		break;
	case PROP_G_TYPE_INT64:
		g_value_set_int64 (value, self->int64prop);
		break;
	case PROP_G_TYPE_LONG:
		g_value_set_long (value, self->longprop);
		break;
	case PROP_G_TYPE_STRING:
		g_value_set_string (value, self->stringprop);
		break;
	case PROP_G_TYPE_UCHAR:
		g_value_set_uchar (value, self->ucharprop);
		break;
	case PROP_G_TYPE_UINT:
		g_value_set_uint (value, self->uintprop); 
		break;
	case PROP_G_TYPE_UINT64:
		g_value_set_uint64 (value, self->uint64prop);
		break;
	case PROP_G_TYPE_ULONG:
		g_value_set_ulong (value, self->ulongprop);
		break;
	case PROP_G_TYPE_UNICHAR:
		g_value_set_uint (value, self->unicharprop);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (instance, prop_id, pspec);
	}
}

static void
class_init (BasicObjectClass *klass)
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

	/* hook gobject vfuncs */
	gobject_class->dispose = instance_dispose;

	basic_object_parent_class = (GObjectClass*) g_type_class_peek_parent (klass);

	gobject_class->set_property = set_property;
	gobject_class->get_property = get_property;

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_BOOLEAN,
		g_param_spec_boolean ("boolean-property", "Test property",
			"For testing only", FALSE,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_CHAR,
		g_param_spec_char ("char-property", "Test property",
			"For testing only", G_MININT8, G_MAXINT8, 'A',
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_DOUBLE,
		g_param_spec_double ("double-property", "Test property",
			"For testing only", G_MINDOUBLE, G_MAXDOUBLE, 2.1,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_FLOAT,
		g_param_spec_float ("float-property", "Test property",
			"For testing only", G_MINFLOAT, G_MAXFLOAT, 3.2,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_INT,
		g_param_spec_int ("int-property", "Test property",
			"For testing only", G_MININT, G_MAXINT, 4,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_INT64,
		g_param_spec_int64 ("int64-property", "Test property",
			"For testing only", G_MININT64, G_MAXINT64, 5,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_LONG,
		g_param_spec_long ("long-property", "Test property",
			"For testing only", G_MINLONG, G_MAXLONG, 6,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_STRING,
		g_param_spec_string ("string-property", "Test property",
			"For testing only", "stringdefault",
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_UCHAR,
		g_param_spec_uchar ("uchar-property", "Test property",
			"For testing only", 0, G_MAXINT8, 'B',
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_UINT,
		g_param_spec_uint ("uint-property", "Test property",
			"For testing only", 0, G_MAXUINT, 8,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_UINT64,
		g_param_spec_uint64 ("uint64-property", "Test property",
			"For testing only", 0, G_MAXUINT64, 9,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_ULONG,
		g_param_spec_ulong ("ulong-property", "Test property",
			"For testing only", 0, G_MAXULONG, 10,
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

	g_object_class_install_property (gobject_class,
		PROP_G_TYPE_UNICHAR,
		g_param_spec_unichar ("unichar-property", "Test property",
			"For testing only", 'a',
			(GParamFlags)(G_PARAM_READWRITE | G_PARAM_CONSTRUCT)));

}

GType
basic_object_get_type (void)
{
        static GType type = 0;
        if (!type) {
                static const GTypeInfo info = {
                        sizeof (BasicObjectClass),
                        NULL,           /* base_init */
                        NULL,           /* base_finalize */
                        (GClassInitFunc) class_init,
                        NULL,           /* class_finalize */
                        NULL,           /* class_data */
                        sizeof (BasicObject),
                        0,              /* n_preallocs */
                        (GInstanceInitFunc) instance_init,
                };
                type = g_type_register_static (G_TYPE_OBJECT, "BasicObject", &info, (GTypeFlags)0);
        }
        return type;
}

