
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gopersist/gtk/gopersist-gtk.h>

int
main (int argc, 
      char *argv[])
{
	GObject *store = NULL;
	GObject *store2 = NULL;
	GopPersist *persist = NULL;
	GopPersist *persist_out = NULL;
	GType type;

	gopersist_gtk_init (&argc, &argv);

	persist = gop_persist_file_new ("persist-gtk-in.xml");
	gop_persist_load (persist);
	store = gop_persist_get_object (persist, "testobj");


	persist_out = gop_persist_file_new ("persist-gtk-out.xml");
	gop_persist_register_object (persist_out, "testobj", store);
	gop_persist_save (persist_out);
	

	g_object_unref (G_OBJECT (persist));
	g_object_unref (G_OBJECT (persist_out));
	if (store)
		g_object_unref (store);
	if (store2)
		g_object_unref (store2);

	return 0;
}
