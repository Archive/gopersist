#!/bin/sh

rm *.xml
./test-basic-xml-writer
./test-basic-xml-reader
xmllint --format persist-in.xml > persist-in_.xml
xmllint --format persist-out.xml > persist-out_.xml
diff -u persist-in_.xml persist-out_.xml
